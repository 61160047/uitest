const jobPostings = [
  {
    _id: 1,
    name: 'Python Programmer',
    lowerBoundAge: 25,
    upperBoundAge: 40,
    gender: 1,
    experience: 2,
    companyId: 1
  },
  {
    _id: 2,
    name: 'Senior Java Programmer',
    lowerBoundAge: 30,
    upperBoundAge: 50,
    gender: 1,
    experience: 2,
    companyId: 2
  },
  {
    _id: 3,
    name: 'Junior Java Programmer',
    lowerBoundAge: 22,
    upperBoundAge: 30,
    gender: 1,
    experience: 3,
    companyId: 1
  },
  {
    _id: 4,
    name: 'Junior Node Programmer',
    lowerBoundAge: 22,
    upperBoundAge: 30,
    gender: 2,
    experience: 2,
    companyId: 1
  }
]
const checkAge = function (age, begin, end) {
  if (age >= begin && age <= end) { return true }
  return false
}

const checkGender = function (gender, postingGender) {
  if (postingGender === 0) {
    return true
  }
  if (postingGender === gender) { return true }
  return false
}

const checkExprience = function (experience, postingExperience) {
  if (experience >= postingExperience) { return true }
  return false
}

const modelGetJobPostingWithId = function (id) {
  return new Promise(function (resolve, reject) {
    if (id <= jobPostings.length) {
      resolve(jobPostings[id - 1])
    } else {
      reject(new Error('No data'))
    }
  })
}

const searchTextJobPosting = function (text) {
  return new Promise((resolve, reject) => {
    const jobs = jobPostings.filter(function (job) {
      const searchText = text.trim().toLowerCase()
      return job.name.toLowerCase().search(searchText) >= 0
    })
    resolve(jobs)
  })
}

const getJobDetail = function (id) {
  return new Promise((resolve, reject) => {
    if (id >= 1 && id <= 4) {
      resolve(jobPostings[id - 1])
    } else {
      reject(new Error('Not found'))
    }
  })
}

const applyJob = function (resume, jobDetail) {
  return new Promise((resolve, reject) => {
    if (!checkAge(parseInt(resume.age), parseInt(jobDetail.lowerBoundAge), parseInt(jobDetail.upperBoundAge))) {
      resolve('Age out of range')
    }
    if (!checkGender(parseInt(resume.gender), parseInt(jobDetail.gender))) {
      resolve('Gender does not meet the conditions')
    }
    if (!checkExprience(parseInt(resume.experience), parseInt(jobDetail.experience))) {
      resolve('Not enough experience')
    }
    resolve('Applied Job')
  })
}
module.exports = {
  checkAge,
  checkGender,
  checkExprience,
  modelGetJobPostingWithId,
  searchTextJobPosting,
  getJobDetail,
  applyJob
}
